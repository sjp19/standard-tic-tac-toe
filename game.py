from unittest import result


def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

def game_over(board, boardnum):
    print_board(board)
    print("GAME OVER")
    print(boardnum, "has won")
    exit()

def is_row_winner(board, row_number):
    board_space = []
    for i in range(3):
        board_space.append((3*row_number)-(i)-1)
    if board[board_space[2]] == board[board_space[1]] and board[board_space[1]] == board[board_space[0]]:
        return True

def is_column_winner(board, column_number):
    board_space = []
    for i in range(3):
        board_space.append((3*column_number)-(3*(i-2))-2*column_number - 1)
    if board[board_space[2]] == board[board_space[1]] and board[board_space[1]] == board[board_space[0]]:
        return True

def is_diagonal_winner(board, diagonal_number):
    board_space = []
    if diagonal_number == 1:
        for i in range(3):
            board_space.append((4*diagonal_number)-(4*(i-2))-4)
        if board[board_space[2]] == board[board_space[1]] and board[board_space[1]] == board[board_space[0]]:
            return True
    if diagonal_number == 2:
        for i in range(3):
            board_space.append(diagonal_number*i + 2)
        if board[board_space[2]] == board[board_space[1]] and board[board_space[1]] == board[board_space[0]]:
            return True

board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if is_row_winner(board, 1):
        game_over(board, board[0])
    elif is_row_winner(board, 2):
        game_over(board, board[3])
    elif is_row_winner(board, 3):
        game_over(board, board[6])
    elif is_column_winner(board, 1):
        game_over(board, board[0])
    elif is_column_winner(board, 2):
        game_over(board, board[1])
    elif is_column_winner(board, 3):
        game_over(board, board[2])
    elif is_diagonal_winner(board, 1):
        game_over(board, board[0])
    elif is_diagonal_winner(board, 2):
        game_over(board, board[2])

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
